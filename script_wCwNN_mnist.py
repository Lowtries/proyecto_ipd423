import tensorflow as tf
from keras.datasets import mnist
from keras.utils import to_categorical
from keras import layers, models, optimizers, callbacks
import matplotlib.pyplot as plt
import numpy as np

# Load MNIST Dataset
(train_images, train_labels),(test_images, test_labels) = mnist.load_data()

def wavelet_activation(x):
    x = tf.dtypes.cast(x, tf.float32)
    return tf.math.cos(1.75*x)*tf.math.exp(-(x**2)/2)

# Preprocess the Data
train_images = train_images.reshape((60000, 28, 28,1)).astype('float32')/255
test_images = test_images.reshape((10000, 28, 28,1)).astype('float32')/255

train_labels = to_categorical(train_labels)
test_labels = to_categorical(test_labels)

# Build Model
model = models.Sequential([
    layers.Input(shape=(28, 28, 1)),
    layers.Conv2D(6,kernel_size=(5, 5), padding='valid', activation=wavelet_activation),
    layers.BatchNormalization(),
    layers.MaxPooling2D(pool_size=(2, 2), strides=(2, 2)),
    layers.Conv2D(12,kernel_size=(5, 5), padding='valid', activation=wavelet_activation),
    layers.BatchNormalization(),
    layers.MaxPooling2D(pool_size=(2, 2), strides=(2, 2)),
    layers.Flatten(),
    layers.Dense(192),
    layers.Dense(10, activation=wavelet_activation),
    layers.Dense(10, activation='sigmoid')
])

learning_rate = 0.01
qty_epochs = 10
custom_opt = optimizers.SGD(learning_rate=learning_rate)

# Compile the model
model.compile(optimizer=custom_opt,
              loss='mse',
              metrics=['accuracy','mean_squared_error'])

# List to store MSE Values
train_mse_values = np.zeros((qty_epochs, len(train_images)//10));

# Custom callback to log MSE after each batch
class LogMSECallback(callbacks.Callback):
    def on_batch_end(self, batch, logs=None):
        current_epoch = self.epoch  # Access the current epoch from the callback
        train_mse_values[current_epoch, batch] = logs.get('loss')

    def on_epoch_begin(self, epoch, logs=None):
        self.epoch = epoch

# Initialize the callback
log_mse_callback = LogMSECallback()

# Train the model
history = model.fit(train_images, train_labels, epochs=qty_epochs, batch_size=10, callbacks=[log_mse_callback])

# Plot MSE During Training
for epoch in range(qty_epochs):
    plt.plot(range(1, len(train_mse_values[epoch]) + 1), train_mse_values[epoch], label=f'Training MSE - Epoch {epoch + 1}')

plt.xlabel('Simulation Time')
plt.ylabel('Mean Squared Error (MSE)')
#plt.legend()
plt.show()